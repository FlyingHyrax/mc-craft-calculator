namespace MekUnitLib

(*
- Make this something useful in FSI - because interactive use is the primary use case!
    - Figure out how the heck FSI works in Visual Studio
    - Am I even using modules correctly?
- Properly account for recipes that produce more than 1 of their item
    - Right now we're just ignoring it, because none of our recipes do that
- How to do unit tests in F#?
    - Obvs this stuff should be unit tested, and it should be "easy" to unit test (no dependency injection)
- Include proper recipes for infuser additives (coal produces 10 carbon, charcoal produces 20 carbon, steel uses ?? carbon)
- Stretch: Create function that lets you print a dependency tree (that would be very cool)
    - The tree could show the machine used in processing
- Mega long term stretch: items with more than one recipe (major model overhaul, something something graphs)
*)

type Machine = string

// what if a machine is a function from inputs to an Item, where item will be a Process with the machine's name attached?

type Item =
    | Material of name : string
    | Craft of inputs : Stack list * count : int
    | Process of machine : Machine * inputs : Stack list * count : int
    static member proc (machine : Machine, input : Item) =
        Process(machine, [Stack.one input], 1)
    static member proc (machine : Machine, inputs : Stack list) =
        Process(machine, inputs, 1)
    member self.add (other : Item) : Stack list =
        let left : Stack = Stack.one self
        let right : Stack = Stack.one other
        left.add right
    member self.stack (count) : Stack =
        { item = self; count = count }
    
    // multiply an item by a number to get a stack
    static member (*) (item : Item, count) =
        item.stack count
    static member (*) (count, item : Item) =
        item.stack count
    // add two items to get a stack list
    static member (+) (left : Item, right : Item) =
        left.add right
    // add an item to a stack list
    static member (+) (left : Stack list, right : Item) =
        (Stack.one right).add left
            
and Stack =
    { item : Item; count : int }
    static member one (item : Item) = 
        { item = item; count = 1 }
    member self.add (other : Stack) =
        if self.item = other.item then
            [ { item = self.item; count = self.count + other.count } ]
        else
            [ self; other ]
    member self.add (other : Item) =
        self.add ( Stack.one other )
    member self.add (others : Stack list) =
        let rec helper (items : Stack list) (other : Stack) =
            match items with
            | first :: rest when first.item = other.item -> { item = first.item; count = first.count + other.count } :: rest
            | first :: rest -> first :: helper rest other
            | [] -> other :: []
        helper others self
    // add items, stacks, and stack lists to create one large stack list
    // (which we can use to create a recipe)
    // add stack to stack
    static member (+) (left : Stack, right : Stack) =
        left.add right
    // add stack to list
    static member (+) (left : Stack list, right : Stack) =
        right.add left
    // add stack to item
    static member (+) (left : Stack, right : Item) =
        left.add right
    static member (+) (left : Item, right : Stack) =
        right.add left

module Crafting =

    let rec materialCost (item : Item) : Map<string, int> =
        let merger (acc : Map<string, int>) (key : string) (value : int) =
            let newVal = if Map.containsKey key acc then Map.find key acc + value else value
            Map.add key newVal acc

        let folder (currentCosts : Map<string, int>) (stack : Stack) : Map<string, int> =
            // must recursively get the cost of the item in the stack,
            // then merge these new costs with the ones we already have:
            Map.fold merger currentCosts (stackCost stack)
            
        match item with
        // the cost of a material is itself
        | Material name -> Map.empty.Add(name, 1)
        // the cost of a craft is the cost of its materials, combined into a single map
        // sort of. doesn't hold when a craft yields more than one item (e.g. planks)
        | Craft (ingredients, count) -> List.fold folder Map.empty ingredients
        // the cost of a process... same as above. Don't need the machine name for costs
        | Process (machine, inputs, count) -> List.fold folder Map.empty inputs

    and stackCost (stack : Stack) : Map<string, int> =
        stack.item |> materialCost |> Map.map (fun _ v -> v * stack.count)

    module Minecraft =
        let glass = Material("glass")
        let wood = Material("wood")
        let cobble = Material("cobblestone")
        let planks = Craft([wood * 1], 4)
        
        let obsidian = Material("obsidian")
        
        let iron = Material("iron")
        let gold = Material("gold")
        let diamond = Material("diamond")

        let lapis = Material("lapis lazuli")
        let coal = Material("coal")
        let redstone = Material("redstone")
    
        let chest = Craft([planks * 8], 1)
        let piston = Craft(iron + redstone + planks * 3 + cobble * 4, 1)

    module Mekanism =
        module Machines =
            let Infuser (additive : Item) (material : Item) : Item =
                Process("Metallurgic Infuser", additive + material, 1)
            let Enricher (input : Item) (output : int): Item =
                Process("Enrichment Chamber", [Stack.one input], output)
            let Crusher (input : Item) : Item =
                Process("Crusher", [Stack.one input], 1)
            let Smelter (input : Item) : Item =
                Process("Energized Smelter", [Stack.one input], 1)

        open Minecraft

        let osmium = Material("osmium")
        let dustDiamond = Machines.Crusher diamond
        let dustObsidian = Machines.Crusher obsidian

        let refinedObsidianDust = Machines.Infuser dustDiamond dustObsidian
        let refinedObsidianIngot = Machines.Smelter refinedObsidianDust

        // making steel
        let enrichedIron = Machines.Infuser coal iron
        let steelBlend = Machines.Infuser coal enrichedIron
        let steel = Machines.Smelter steelBlend

        // alloys
        let enrichedAlloy = Machines.Infuser redstone iron
        let reinforcedAlloy = Machines.Infuser dustDiamond enrichedAlloy
        let atomicAlloy = Machines.Infuser refinedObsidianDust reinforcedAlloy

        // components
        let energyTablet = Craft(2 * enrichedAlloy + 3 * gold + 4 * redstone, 1)
        let steelCasing = Craft(4 * steel + 4 * glass + osmium, 1)
        let teleportationCore = Craft(diamond + 2 * gold + 2 * atomicAlloy + 4 * lapis, 1)
        let basicControlCircuit = Machines.Infuser redstone osmium

        // devices
        let logisticalSorter = Craft(iron * 7 + piston + basicControlCircuit, 1)
        let personalChest = Craft(steel * 5 + chest * 2 + glass * 1 + basicControlCircuit * 1, 1)
        let robit = Craft(steel + atomicAlloy + personalChest + energyTablet * 2 + refinedObsidianIngot * 2, 1)

        let digitalMiner = Craft(steelCasing + basicControlCircuit + robit + 2 * logisticalSorter + atomicAlloy * 2 + teleportationCore * 2, 1)
