﻿// Learn more about F# at http://fsharp.org

open System
open MekUnitLib

[<EntryPoint>]
let main argv =
    printfn "Resources needed for Digital Miner (maybe)"
    
    Crafting.materialCost Crafting.Mekanism.digitalMiner
        |> Map.toList
        |> List.sortBy (fun (name, count) -> -count)
        |> List.map (fun (name, count) -> sprintf "%s %d" name count)
        |> String.concat "\n"
        |> printfn "%s"
    
    Console.ReadKey |> ignore
    0 // return an integer exit code
